Remastering BlankOn
===================

1. Sesuaikan ``conf``.
2. Jalankan ``sh 0-tools.sh`` untuk mengunduh aplikasi yang diperlukan.
3. Jalankan ``sh 1-extract.sh`` untuk mengekstrak file ISO.
4. Jalankan ``sh 2-chroot.sh`` untuk masuk ke lingkungan chroot. Di sini mulailah
   menambah aplikasi.
5. Jalankan ``sh 3-pack.sh`` untuk mengemasnya ke sebuah squash filesystem.
6. Jalankan ``sh 4-mkiso.sh`` untuk mengemasnya menjadi file ISO.
7. Selesai.
