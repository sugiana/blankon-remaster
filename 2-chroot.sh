#!/bin/sh

cp /etc/resolv.conf root/etc
chroot root locale-gen id_ID.utf8
chroot root mount -t proc none /proc
chroot root mount -t sysfs none /sys
mount -o bind /dev root/dev
chroot root mount -t devpts none /dev/pts
chroot root
rm -f root/etc/resolv.conf
chroot root apt-get clean
chroot root umount /dev/pts 
umount root/dev
chroot root umount sys
chroot root umount proc
chroot root rm -rf /tmp/* /root/.bash_history
chmod +w cdrom/live/filesystem.manifest
chroot root dpkg-query -W --showformat='${Package} ${Version}\n' > cdrom/live/filesystem.manifest
cp cdrom/live/filesystem.manifest cdrom/live/filesystem.manifest-desktop
sed -ie '/ubiquity/d' cdrom/live/filesystem.manifest-desktop
