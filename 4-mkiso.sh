. ./conf


#cat << @EOF > /tmp/temp.cfg
#set prefix=/boot
#configfile /boot/grub/grub.cfg
#@EOF
#grub-mkimage -p /boot -o /tmp/core.img -O i386-pc biosdisk iso9660 linux configfile -c /tmp/temp.cfg
#cat /tmp/core.img > cdrom/boot/grub/eltorito.img
genisoimage -v -A BlankOnCDFactory -p BlankOn -publisher BlankOn -V "${LABEL}" -no-emul-boot -boot-load-size 4 -boot-info-table -r -b boot/grub/eltorito.img -o $TARGET -J -l -cache-i cdrom
#rm -f /tmp/temp.cfg /tmp/core.img
echo "Source "`ls -l $SOURCE | awk '{print $5}'`
echo "Target "`ls -l $TARGET | awk '{print $5}'`
