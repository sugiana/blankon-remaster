. ./conf
mkdir -p /tmp/cdrom /tmp/root
mount -o loop $SOURCE /tmp/cdrom
rsync --exclude 'filesystem.squashfs' -a /tmp/cdrom/ cdrom/ 
mount -o loop -t squashfs /tmp/cdrom/live/filesystem.squashfs /tmp/root
cp -a /tmp/root .
umount /tmp/root
umount /tmp/cdrom
